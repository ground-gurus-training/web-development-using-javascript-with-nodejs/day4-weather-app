import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { WeatherService } from '../services/weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  weather: any;

  constructor(
    private weatherService: WeatherService
  ) {
  }

  async ngOnInit() {
    this.weather = await this.weatherService.getWeather('Manila').toPromise();
  }

}
