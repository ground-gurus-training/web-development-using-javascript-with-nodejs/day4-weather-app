import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private url = 'http://api.openweathermap.org/data/2.5/weather';
  private apiKey = '96c20bd7450059c8d1ba71d6a7dc73a4';

  constructor(
    private http: HttpClient
    ) {
  }

  getWeather(query: string): Observable<any> {
    return this.http.get(`${this.url}?q=${query}&appid=${this.apiKey}`);
  }
}
